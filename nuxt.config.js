const strapiBaseUri = process.env.API_URL || "http://localhost:1337";

export default {
  target: 'static',
  env: {
    strapiBaseUri,
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#fff'
  },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    
  ],

  ssr: false,

  buildModules: [
    '@nuxtjs/fontawesome',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/google-fonts',
    '@nuxtjs/moment'
  ],

  googleFonts: {
    families: {
      Mulish: [300, 500],
    }
  },

  fontawesome: {
    icons: {
      regular: true,
      brands: true,

      }
  },
  sitemap : {
    hostname: 'https://elkemoras.be',
    exclude: [
      '/secret',
      '/blog'
    ],
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    "@nuxtjs/markdownit",
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    '@nuxtjs/strapi',
    ['@nuxtjs/google-analytics', {
      id: 'UA-161772338-1'
    }],
    '@nuxtjs/sitemap'
  ],
  strapi: {
    url: strapiBaseUri,
    entities: [
      {
        name: "articles",
        type: "collection",
      },
      {
        name: "categories",
        type: "collection",
      },
    ],
  },
  markdownit: {
    preset: "default",
    linkify: true,
    breaks: true,
    injected: true,
    html: true,
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
