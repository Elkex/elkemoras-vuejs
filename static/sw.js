importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/1b06a4b.js",
    "revision": "fc4c0243688aeb5c5c02f858b3e50183"
  },
  {
    "url": "/_nuxt/321016f.js",
    "revision": "de9ff14a6a081cd0ab6e8c84ced62fed"
  },
  {
    "url": "/_nuxt/3861a4d.js",
    "revision": "a14dfe4e7900010a514675e75085d001"
  },
  {
    "url": "/_nuxt/3c84a63.js",
    "revision": "fd22e5b32280c5d74e3c74421e47e84c"
  },
  {
    "url": "/_nuxt/837902b.js",
    "revision": "c90867dfb04572b6de176fee716dcb5d"
  },
  {
    "url": "/_nuxt/85932e0.js",
    "revision": "253ff712d3586234d5c9e63599c035fc"
  },
  {
    "url": "/_nuxt/8609fb2.js",
    "revision": "00924b1b0a237955124c81cfba9a6edb"
  },
  {
    "url": "/_nuxt/ae2df7f.js",
    "revision": "7318244694d5448339f1e20968649bcf"
  },
  {
    "url": "/_nuxt/b575f99.js",
    "revision": "a254dad4e4a2176476f02599c176d9e2"
  },
  {
    "url": "/_nuxt/ed48f5c.js",
    "revision": "ed5a8afb61bc30addcd46b8804e5193c"
  }
], {
  "cacheId": "elkemoras",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
